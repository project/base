<?php

namespace Drupal\base\Commands;

use Drush\Drush;
use Drush\Commands\DrushCommands;
use \RecursiveDirectoryIterator;
use \RecursiveIteratorIterator;
use \FilesystemIterator;

/**
 * Base Drush commandfile.
 *
 */
class BaseCommands extends DrushCommands {
  /**
   * Helps symlinking a valid folder containing an exported set of Drupal configs.
   *
   * @param string $basePath
   *   Base path to start searching from. (Defaults to ./)
   *
   * @command base:symlink
   * @aliases blink
   * @usage base:symlink ../ 
   *   Starts looking under the web root [../] for config folders to symlink.
   * @usage base:symlink ../ --depth=2
   *   Use a lower depth value to scan faster. (Default is 5)
   * @usage base:symlink ../ --match=stage
   *   Use a match value to to filter out folders you don't need. 
   */
  public function symlinkConfig($basePath = '../', $options = ['depth' => 5, 'match' => '']) {

    $path = [];

    $module_handler = \Drupal::service('module_handler');
    $path['module_sync'] = $module_handler->getModule('base')->getPath() .'/config';

    $this
      ->logger()
      ->notice(dt(
        'Symlink will be created as [@path/sync] and replace any existing link.',
        ['@path' => $path['module_sync']]
      ));

    $directory = new RecursiveDirectoryIterator($basePath, FilesystemIterator::SKIP_DOTS);
    $iterator = new RecursiveIteratorIterator($directory);
    $iterator->setMaxDepth($options['depth']);
    $config_directory = array();
    
    foreach ($iterator as $info) {
      if ($info->getFilename() === 'system.site.yml') {
        $config_directory[] = dirname($info->getPathname());
      }
    }

    if (!empty($options['match'])) {
      $match = $options['match'];
      $match = array_filter($config_directory, function($element) use ($match) {
        return ( strpos($element, $match) !== false );
      });

      if (empty($match)) {
        $this
          ->logger()
          ->error(dt(
            'No config folder found matching criteria. Nothing to symlink.'
          ));
        return;
      }

      # Reorder filtered list of directory to have ordinal keys 0, 1, etc needed for choice input.
      $config_directory =[];
      foreach ($match as $key => $value) {
        $config_directory[] = $value;
      }
    }

    $path['selected_config'] = $config_directory[
      $this
        ->io()
        ->choice(dt('Choose source config folder:'), $config_directory, $config_directory[0])
    ];
    
    $path['selected_config_relative'] = $this->getRelativePath($path['module_sync'], $path['selected_config']);
    $shell_command = 'ln -sfn ' . $path['selected_config_relative'] . ' ' . $path['module_sync'] . '/sync';
    $this
      ->logger()
      ->success(dt(
        'Executing [@command]',
        ['@command' => $shell_command]
      ));
    $this
      ->processManager()
      ->shell($shell_command)
      ->run();
  }

  /**
   * Returns the relative path from the origin path to the destination one.
   */
  private function getRelativePath($from, $to) {
    $dir = explode(DIRECTORY_SEPARATOR, is_file($from) ? dirname($from) : rtrim($from, DIRECTORY_SEPARATOR));
    $file = explode(DIRECTORY_SEPARATOR, $to);

    while ($dir && $file && ($dir[0] == $file[0])) {
        array_shift($dir);
        array_shift($file);
    }
    
    return str_repeat('..' . DIRECTORY_SEPARATOR, count($dir)) . implode(DIRECTORY_SEPARATOR, $file);
  }
}