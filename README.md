# Base Install Profile

Dead simple install profile that does nothing but remove the need to create your own custom profile in order to use the new convention available in Drupal 8.6 to install a new site from an exported configuration to your install profile config/sync folder.

#### Links
- [Installing Drupal from configuration](https://www.drupal.org/node/2897299)
- [Install a site from config if the config directory is set in settings.php](https://www.drupal.org/project/drupal/issues/2980670)

## How to use

#### 1. Add this profile as a dependency to your project using composer

```
composer require drupal/base:^2.0
```

#### 2. Symlink your config/sync to your base/config/sync folder

A Drush 9 command is available to help you symlink your configs with the base profile.

It should be fairly easy to use, but please review its usage using 

```
drush base:symlink --help
```

_You can still do the symlink manually and the following might be still be useful._

If you've used the drupal/composer ([https://github.com/drupal-composer/drupal-project](https://github.com/drupal-composer/drupal-project)) to create your new Drupal site, symlink your configuration export should be accomplished using this command:

```
# From your project base folder
ln -s ../../../../../config/sync web/profiles/contrib/base/config/sync
```

_Adjust the first part of th command if your folder is located or named diffrently_

#### 3. Run drush site-install to have your site installed using your exported configuration

```
drush si base
```
